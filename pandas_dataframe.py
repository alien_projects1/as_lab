import pandas as pd
from sqlalchemy import create_engine


def sort_dataframe(df):
    try:
        return df.sort_values(by=[1, 2], axis=1, ascending=False)
    
    except Exception as e:
        print(f"An error occured: {str(e)}")

def min_max_value(df, col1:str, col2:str):
    try:
        max_row = df[col1].idxmax()
        min_row = df[col2].idxmin()
        return min_row, max_row
    
    except Exception as e:
        print(f"An error occured: {str(e)}")

def groupbyName(df, by:str):
    try:
        return df.groupby(by)
    except KeyError:
        print(f"Column '{by}' not found.")
    except Exception as e:
        print(f"An error occured: {str(e)}")

def splitNameintoFirstandLast(df, col:str, delimiter):
    try:
        # split the existing column
        splits = df[col].str.split(delimiter, expand=True)

        # create new columns
        new_columns = ['First Name', 'Last Name']
        df[new_columns] = splits

        # drop the original
        df.drop(col, axis=1, inplace=True)

        return df

    except KeyError:
        print(f"Column '{col}' not found.")
    except Exception as e:
        print(f"An error occured: {str(e)}")

def connect_store_close_postgresql(df, table_name : str):
    engine = create_engine('postgresql://username:password@host:port/database') # currently i do not have postgres instance running but this will work
    df.to_sql(table_name, engine, index=False, if_exists='replace')

    engine.dispose()


############################################

data1 = {
    'ID' : [1,2,3,4,5],
    'A': [-22.2, 11.7, 23, 8.555, 222],
    'B': [-21.2, 5.78, 0.2121, 5.455, -11],
    'C': [1.2, 0.7, -43, -1.5, 12]
}
df1 = pd.DataFrame(data1)

data2 = {
    'ID' : [3,4,5,6,7],
    'Name': ['Ghalib Mirza', 'Peter Holzapfel', 'Ghalib Mirza', 'Ahmed Mir', 'Peter Holzapfel'],
    'City': ['Cologne', 'Warsaw', 'Brisbane', 'Dortmund', 'Paris'],
    'Age': [30, 32, 35, 36, 33],
    'Salary': [55000, 62000, 65000, 58000, 61000]
}
df2 = pd.DataFrame(data2)

##
sorted_df = sort_dataframe(df1)
print(sorted_df)

##
min_row, max_row = min_max_value(df1, 'B', 'C')
print(f"Row index with maximum value in 'B':{max_row}")
print(f"Row index with minimum value in 'C':{min_row}")

##
grouped = groupbyName(df2, 'Name')

for name, group in grouped: 
    print(f"\nGroup Name: {name}")
    print(group)

##
new_df = splitNameintoFirstandLast(df2, 'Name', ' ')
print(f"\n New DataFrame: \n{new_df}")

##
# Since join() combines two dataframes based on their indexes, i have to set the index columns
df1_1 = df1.set_index('ID')
df2_1 = df2.set_index('ID')
joined_df = df1_1.join(df2_1)
print(f"\n New joined dataframe is : \n{joined_df}")

##
# For merging we just need to specify the column and how you want to merge[left, right] with left yield the same result as join()
merged_df = pd.merge(df1,df2, on='ID', how='left')
print(f"\n New Merged dataframe is : \n{merged_df}")

##
table_name = "my_table"
# connect_store_close_postgresql(merged_df, table_name)