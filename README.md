# User Manual

## How to:
After cloning the repo source the venv named "as_lab_venv" and install all the required dependencies via "requirements.txt"
## Knowledge Exercise
To look at the answer from the knowledge excercise please look at the [knowledgeX.md](https://gitlab.com/alien_projects1/as_lab/-/blob/main/knowledgeX.md) markdown file.

## ShoppingCart Exercise
To explore the shopping cart problem look at the [shoppingcart.py](https://gitlab.com/alien_projects1/as_lab/-/blob/main/shoppingcart.py).


## Pandas Library
To checkout all the relavant exercises related to Pandas library look at the [pandas_dataframe.py](https://gitlab.com/alien_projects1/as_lab/-/blob/main/pandas_dataframe.py).