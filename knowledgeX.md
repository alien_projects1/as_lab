# AS Lab

## Knowledge Exercise

#### Q1 - After a purchase, Amazon shows you products that customers with your product have also purchased. Which procedure can be used to generate such recommendations?
Generally to create a recommendations based on "customers who bought this also bought that" system, the technique called  collaborative filltering is used. In which we have to find similat buying pattern from different customers and from a similar geo location and also may be similar age group along with other factors.

#### Q2 - How can missing values be supplemented / filled in during data preprocessing? Describe (briefly!!) two possible approaches.
So, there are many imputation techniques to deal with missing data problem. one of which is imputation with statistical methods like mean/mode/median.

#### Q3 - Which advantages can NoSQL databases offer compared to classical relational databases?
To my knowledge, the advantages NoSQL DB have over classical RDB are Scalability, High performance when it comes to Big data, Flexible schematic designs.

#### Q4 - You have received a dataset. You realize that 25% of the data is wrong and 15% of the data is missing. How do you describe your problem? What are your next steps to solve the problem?
Since, its not mention the size of the dataset, and nothing is said about how computationally expensive is to collect the new data, hence my first approach would be to get rid of NULLS and erronous data points. Second approach would be to impute the nulls with few different statistical methods.

#### Q5 - Which possibilities do you have to visualize data problems? Which ones do you recommend to explain a data problem?
There are plenty of possibilities to visualize the data, most commonly used are scatter plot, box plot, histograms, distribution graphs, heatmaps and line charts. You can also use visualization tools for visualization like Tableau or PowerBI.

#### Q6 - Explain Inheritance in Python with an coding example.
```
# Inheritance Example 
class Person:
    def __init__(self, name):
        self.name = name

    def introduce(self):
        print(f"Hello, my name is {self.name}.")


class Profession(Person):
    def __init__(self, name, profession):
        super().__init__(name)
        self.profession = profession

    def introduce(self):
        print(f"Hello, my name is {self.name}, and I am a {self.profession}.")


# Creating instances of the classes
person = Person("Ghalib Mirza")
prof = Profession("Ghalib", "Data Scientist")

# Call the introduce method on instances
person.introduce()        # Output: Hello, my name is Ghalib Mirza.
prof.introduce()  # Output: Hello, my name is Ghalib, and I am a Data Scientist.

```
So in above code sample class "Profession" inherits the "Person" class and overrides the introduce method.

#### Q7 - What is “self” in Python and where are its use cases? Give an explanation and coding example.
Its the conventional name given to the first parameter of a method in a class. By using self, you can access and modify instance variables and call other methods within the class. It acts as a reference to the instance itself, which allows you to work with the specific data of a specific instance.

#### Q8 - What is the process of compilation and linking in python?
In Puthon, the compilation and linking task are performed by python interpreter automatically (interpreted approach), unlike C/C++. It executes the code directly, doesnt need to compile it to machine code.