from itertools import combinations

def get_combinations(packages, max_weight):
    valid_combinations = []
    for r in range(1, len(packages) + 1):
        for combination in combinations(packages, r):
            total_weight = sum(package['weight'] for package in combination)
            if total_weight <= max_weight:
                valid_combinations.append(combination)
    return valid_combinations

def find_lowest_price_combination(combinations):
    lowest_price = float('inf')
    lowest_price_combination = None
    for combination in combinations:
        total_price = sum(package['price'] for package in combination)
        if total_price < lowest_price:
            lowest_price = total_price
            lowest_price_combination = combination
    return lowest_price_combination

# Initial setup
packages = [
    {'id': 1, 'price': 24, 'weight': 8.08},
    {'id': 2, 'price': 28.3, 'weight': 5.7},
    {'id': 3, 'price': 210, 'weight': 7.8},
    {'id': 4, 'price': 10.2, 'weight': 3.2},
    {'id': 5, 'price': 4.4, 'weight': 2.1},
    {'id': 6, 'price': 40.5, 'weight': 11},
    {'id': 7, 'price': 17, 'weight': 4.44},
    {'id': 8, 'price': 35.6, 'weight': 6.4},
    {'id': 9, 'price': 82.7, 'weight': 16.3},
    {'id': 10, 'price': 14.6, 'weight': 1.7}
]
max_weight = 50

# 1. Get all combinations that fit into the shopping cart
all_combinations = get_combinations(packages, max_weight)
num_combinations = len(all_combinations)
print(f"Number of combinations that fit into the shopping cart: {num_combinations}")

# 2. Find the combination with the lowest price and the most packages
lowest_price_combination = find_lowest_price_combination(all_combinations)
num_packages = len(lowest_price_combination)
lowest_price = sum(package['price'] for package in lowest_price_combination)
print(f"Lowest price combination with the most packages: {lowest_price_combination}")
print(f"Number of packages in the combination: {num_packages}")
print(f"Total price: {lowest_price}")

# 3. Reduce weight capacity to 35 kg and exclude two packages
max_weight = 35
excluded_packages = [7, 10]  # Packages with IDs 7 and 10 are excluded

# Remove excluded packages
updated_packages = [package for package in packages if package['id'] not in excluded_packages]

# Get all combinations with updated weight capacity
updated_combinations = get_combinations(updated_packages, max_weight)

# Find the combination with the lowest price and the most packages
updated_lowest_price_combination = find_lowest_price_combination(updated_combinations)
updated_num_packages = len(updated_lowest_price_combination)
updated_lowest_price = sum(package['price'] for package in updated_lowest_price_combination)
print(f"\nAfter reducing weight capacity to 35 kg and excluding two packages:")
print(f"Lowest price combination with the most packages: {updated_lowest_price_combination}")
